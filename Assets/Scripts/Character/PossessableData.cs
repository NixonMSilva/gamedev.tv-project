using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Character
{
    public class PossessableData : MonoBehaviour
    {
        [SerializeField] private int _possessablesRemainingCount = 0;
        
        public int PossessableCount
        {
            get => _possessablesRemainingCount;
        }

        public void AddValue (int delta)
        {
            _possessablesRemainingCount += delta;

            // Game over condition: No possessables left

            if (_possessablesRemainingCount <= 0)
            {
                GameManager.instance.Defeat();
            }
        }

        [ContextMenu("Count Possessables")]
        void CountPossessables ()
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

            // Starts with the player

            _possessablesRemainingCount = 1;

            foreach (GameObject enemy in enemies)
            {
                DeathController current = enemy.GetComponent<DeathController>();
                if (current.IsPossessable)
                {
                    //Debug.Log(current);
                    _possessablesRemainingCount++;
                }
            }
        }

    }
}

