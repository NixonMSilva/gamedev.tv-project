using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy
{
    public class EnemyDetection : MonoBehaviour
    {
        private Collider2D _detectionRadius;

        [SerializeField] private EnemyAI _enemyAI;

        private void Awake ()
        {
            // Caching component data

            _detectionRadius = GetComponent<CircleCollider2D>();
            _enemyAI = GetComponentInParent<EnemyAI>();
        }

        private void OnTriggerEnter2D (Collider2D collision)
        {
            // Return if there's no active AI component

            if (_enemyAI == null) { return; }

            // Checks if the player is in detection range

            if (collision.gameObject.CompareTag("Player"))
            {
                _enemyAI.CanSeePlayer = true;
                // _enemyAI.CurrentPlayer = collision.gameObject;
            }
        }

        private void OnTriggerExit2D (Collider2D collision)
        {
            // Return if there's no active AI component

            if (_enemyAI == null) { return; }

            // Checks if the player is outside detection range

            if (collision.gameObject.CompareTag("Player"))
            {
                _enemyAI.CanSeePlayer = false;
            }
        }
    }
}