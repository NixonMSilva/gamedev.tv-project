using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Character;

namespace Enemy
{
    public class EnemyAI : MonoBehaviour, IDetectable
    {
        private MovementController _movement;
        private WeaponController _weapon;
        private DeathController _death;
        private AimController _aim;

        private Rigidbody2D _detectionRadius;
        private bool _canSeePlayer = false;

        [SerializeField] private float _minPursueDistance = 1.5f;
        private float _minPursueDistanceSquared;

        [SerializeField] private GameObject _currentPlayer;

        [SerializeField] private float _weaponCooldown = 0.5f;
        private float _weaponCooldownElapsed;

        [SerializeField] private bool _canMove = true;

        [SerializeField] private LayerMask _jumpPointLayer;

        public bool CanSeePlayer
        {
            get => _canSeePlayer;
            set { _canSeePlayer = value; }
        }

        public GameObject CurrentPlayer
        {
            get => _currentPlayer;
            set { _currentPlayer = value; }
        }

        // Subscribes the class to listen to any changes on the player object
        private void OnEnable ()
        {
            //Debug.Log("Instance status on EnemyAI.cs:" + GameManager.instance);

            if (GameManager.instance != null)
            {
                GameManager.instance.onPlayerChange += UpdatePlayerReference;
            }
        }

        private void OnDisable ()
        {
            if (GameManager.instance != null)
                GameManager.instance.onPlayerChange -= UpdatePlayerReference;
        }

        private void Awake ()
        {
            // Caching component data

            _movement = GetComponent<MovementController>();
            _weapon = GetComponent<WeaponController>();
            _death = GetComponent<DeathController>();
            _aim = GetComponent<AimController>();

            // Caching weapon data

            _weaponCooldown = _weapon.WeaponCooldown;
        }

        private void Start()
        {
            _minPursueDistanceSquared = _minPursueDistance * _minPursueDistance;

            _currentPlayer = GameManager.instance.PlayerObject;
        }

        private void Update ()
        {
            // Doesn't do anything if it's dead

            if (_death.IsDead) { return; }

            // Doesn't do anything if the player is dead

            if (!GameManager.instance.IsPlayerAlive) { return; }

            if (!CanSeePlayer)
            {
                // Perform movement pattern

                PerformMovePattern();
                return;
            
            }

            FlipTowardsPlayer();

            AimTowardsPlayer();

            FireWeapon();
        }

        private void FixedUpdate ()
        {
            // Doesn't do anything if it's dead

            if (_death.IsDead) { return; }

            // Doesn't do anything if the player is dead

            if (!GameManager.instance.IsPlayerAlive) { return; }

            // Moves on FixedUpdate because physics

            if (CanSeePlayer && _canMove)
            {
                MoveTowardsPlayer();
            }
        }

        private void MoveTowardsPlayer ()
        {
            // Does not pursue player if it's too close
            if (Vector2.SqrMagnitude(_currentPlayer.transform.position - gameObject.transform.position) <= (_minPursueDistanceSquared))
            {
                _movement.Stop();
                return;
            }

            Vector2 directionToPlayer = _currentPlayer.gameObject.transform.position - gameObject.transform.position;
            _movement.PerformMove(directionToPlayer.normalized);

            // Verify if the enemy has to jump to follow the player

            EvaluateJump();
        }

        private void EvaluateJump ()
        {
            // Jumps if the player is currently at least 1 tile above the enemy
            // and if the enemy is at a Jump Point

            if (_currentPlayer.gameObject.transform.position.y >= (gameObject.transform.position.y + 1f))
            {
                Collider2D[] jumpPoints = Physics2D.OverlapCircleAll(transform.position, 1f, _jumpPointLayer);

                if (jumpPoints != null && jumpPoints.Length > 0)
                {
                    _movement.PerformJump();
                }
            }
        }

        private void PerformMovePattern ()
        {

        }

        private void FlipTowardsPlayer ()
        {
            if (_currentPlayer == null)
                return;

            // Flips towards player 

            if (_currentPlayer.transform.position.x > transform.position.x)
            {
                _movement.Flip(false);
                _weapon.Front = Vector2.right;
            }
            else if (_currentPlayer.transform.position.x < transform.position.y)
            {
                _movement.Flip(true);
                _weapon.Front = Vector2.left;
            }
        }

        private void AimTowardsPlayer ()
        {
            float angle = Vector2.SignedAngle(_weapon.Front, _currentPlayer.transform.position - transform.position);
            _aim.UpdateAim(angle, _movement.IsFlipped);
        }

        private void FireWeapon ()
        {
            // Check if the weapon cooldown time has fully elapsed 
            // before firing

            if (_weaponCooldownElapsed <= 0f)
            {
                _weapon.Fire();
                _weaponCooldownElapsed = _weaponCooldown;
            }
            else
            {
                _weaponCooldownElapsed -= Time.deltaTime;
            }
        }

        private void UpdatePlayerReference (GameObject newPlayer)
        {
            _currentPlayer = newPlayer;
        }

        public void DetectDamage(GameObject source)
        {
            // Checks if the source of the damage isn't the new player, if it is
            // set sights on it

            if (source.Equals(GameManager.instance.PlayerObject))
            {
                _canSeePlayer = true;
            }
        }
    }
}