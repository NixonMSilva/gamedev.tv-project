﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Character;

namespace Enemy
{
    public class BossController : MonoBehaviour
    {
        private WeaponController _weaponController;
        private HealthController _healthController;

        private void Awake()
        {
            _weaponController = GetComponentInParent<WeaponController>();
            _healthController = GetComponentInParent<HealthController>();
        }

        private void Start()
        {
            _weaponController.CanFire = false;
        }

        private void OnEnable ()
        {
            _healthController.OnDeath += BossDeath;
        }

        private void OnDisable ()
        {
            _healthController.OnDeath -= BossDeath;
        }

        public void AllowFiringEvent ()
        {
            _weaponController.CanFire = true;
        }

        private void BossDeath ()
        {
            AudioManager.instance.PlaySound("Explosion_Wozinsky Death");
            Invoke("CallEndGame", 2f);
        }

        private void CallEndGame ()
        {
            SceneManager.LoadScene("EndScreen");
        }
        
    }
}