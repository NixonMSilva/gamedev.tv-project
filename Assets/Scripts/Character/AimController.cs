using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Character
{
    public class AimController : MonoBehaviour
    {
        [SerializeField] private GameObject _wpnObject;
        private WeaponController _wpnController;

        private float _clampedAngle;

        private void Awake ()
        {
            // Caching component data

            _wpnController = GetComponent<WeaponController>();
        }

        public void UpdateAim (float angle, bool isFlipped)
        {
            // Rotates the weapon according to the position of the mouse on the screen
            // clamping it to preset angles (horizontal, vertical, diagonal)

            if (angle > 60f || angle < -60f)
            {
                if (isFlipped == false)
                {
                    _clampedAngle = 90f;
                }
                else
                {
                    _clampedAngle = -90f;
                }
                _wpnController.Angle = new Vector2(0f, 1f);
            }
            else if (angle <= 60f && angle > 30f)
            {
                _clampedAngle = 45f;
                if (isFlipped)
                {
                    _wpnController.Angle = new Vector2(transform.localScale.x, -1f);
                }
                else
                {
                    _wpnController.Angle = new Vector2(transform.localScale.x, 1f);
                }
            }
            else if (angle < 30f && angle >= -30f)
            {
                _clampedAngle = 0f;
                _wpnController.Angle = new Vector2(transform.localScale.x, 0f);
            }
            else if (angle < -30f && angle >= -60f)
            {
                _clampedAngle = -45f;
                if (isFlipped)
                {
                    _wpnController.Angle = new Vector2(transform.localScale.x, 1f);
                }
                else
                {
                    _wpnController.Angle = new Vector2(transform.localScale.x, -1f);
                }
            }

            Quaternion rotation = Quaternion.Euler(0f, 0f, _clampedAngle);
            _wpnObject.transform.rotation = rotation;
        }
    }

}