using Character;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GhostController : MonoBehaviour, IInputMoveable
{
    public bool isActive = true;

    private Rigidbody2D _rb;
    private MovementController _movement;

    private PlayerInput _playerInput;

    private Vector2 _rawInput;

    private List<DeathController> _possessableList = new List<DeathController>();

    private bool _possessionLock = false;

    [SerializeField] private float _possessionRadius = 0.75f;

    private void Awake ()
    {
        // Component caching

        _rb = GetComponent<Rigidbody2D>();
        _movement = GetComponent<MovementController>();

        _playerInput = GetComponent<PlayerInput>();
        _playerInput.neverAutoSwitchControlSchemes = true;
    }

    private void Update()
    {
        // Process possession after acquiring list size
        // CheckPossession();

        if (_possessableList.Count > 0)
        {
            Invoke("ProcessPossession", 0.05f);
        }
    }

    /*
    private void CheckPossession()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 
            _possessionRadius, LayerMask.NameToLayer("Damageable"));

        Debug.Log("Colliders: " + colliders.Length);

        foreach (Collider2D col in colliders)
        {
            DeathController deathController = col.gameObject.GetComponent<DeathController>();

            if (deathController != null)
            {
                if (deathController.IsPossessable && !deathController.IsDead)
                {
                    // Makes the ghost possess the character if possessable
                    // destroys the ghost shortly afterwards

                    _possessableList.Add(deathController);
                }
            }
        }
    } */

    private void FixedUpdate ()
    {
        // Performs movement according to inputs

        _movement.PerformMove(_rawInput);
    }

    //Used by the input system 
    public void OnMove (InputValue value)
    {
        // Check if the player can move (it's not dashing nor the object is inactive)
        if (!isActive) { return; }

        _rawInput = value.Get<Vector2>();
    }

    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Damageable"))
        {
            DeathController deathController = collision.GetComponent<DeathController>();

            if (deathController != null)
            {
                if (deathController.IsPossessable && !deathController.IsDead)
                {
                    // Makes the ghost possess the character if possessable
                    // destroys the ghost shortly afterwards

                    _possessableList.Add(deathController);
                }
            }
        }
    }

    private void ProcessPossession ()
    {
        // Process the possession, this method exists to avoid the player
        // possessing more than one character at once, all the possessables
        // are cached on a list and then this method is invoked shortly afterward
        // making the player possess only the first character on the list

        if (_possessionLock) { return; }

        _possessionLock = true;

        if (_possessableList.Count > 1) Debug.Log("More than 1 possessable on list");

        _possessableList[0].Possess();

        // Sets the new player object on the GameManager to propagate the updated
        // information

        GameManager.instance.PlayerObject = _possessableList[0].gameObject;

        // Destroys the ghost object a few miliseconds afterwards

        Destroy(gameObject, 0.2f);
    }

}
