using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Character
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerController : MonoBehaviour, IInputMoveable
    {
        [SerializeField] private float _dashForce = 50f;
        [SerializeField] private Collider2D _feet;

        public bool isActive = true;
        public bool isFlipped = false;

        private Vector2 _moveDirection;
        private Vector2 _rawInput;
        private Vector2 _mousePosition;

        private bool _isJumping = false;
        private bool _isDashing = false;
        private bool _canFire = true;

        private float _dashTimeTotal = 0.2f;
        private float _dashTimeElapsed = 0.2f;

        [SerializeField] private MovementController _movement;
        [SerializeField] private AimController _aim;

        private WeaponController _wpnController;

        private float _angle = 0f;

        private Rigidbody2D _rb;
        private SpriteRenderer _sprite;

        private Camera _camera;

        private void Awake()
        {
            // Component caching
            _rb = GetComponent<Rigidbody2D>();
            _sprite = GetComponentInChildren<SpriteRenderer>();
            _wpnController = GetComponent<WeaponController>();
            _movement = GetComponent<MovementController>();
            _aim = GetComponent<AimController>();

            _camera = Camera.main;
        }

        private void FixedUpdate ()
        {
            // Performs movement according to inputs
            _movement.PerformMove(_rawInput);

            // Updates the jump if enabled 
            if (_isJumping)
            {
                _movement.PerformJump();
                _isJumping = false;
            }

            // Updates the dashing if enabled
            UpdateDash();

            // Updates the aim point
            _aim.UpdateAim(_angle, _movement.IsFlipped);
        }

        // Used by the input system 
        public void OnMove (InputValue value)
        {
            // Check if the player can move (it's not dashing nor the object is inactive)

            if (!isActive) { return; }
            if (_isDashing) { return; }

            _rawInput = value.Get<Vector2>();
        }

        // Used by the input system
        private void OnJump (InputValue value)
        {
            // Check if the player can jump (is touching ground, not dashing and the object is active)

            if (!isActive) { return; }
            if (_isDashing) { return; }

            _isJumping = true;
        }
    
        // Used by the input system
        private void OnDash (InputValue value)
        {
            // Check if the player can dash (it's not already dashing nor the object is inactive)

            if (!isActive) { return; }
            if (_isDashing) { return; }

            _isDashing = true;
        }

        // Used by the input system
        private void OnAim (InputValue value)
        {
            // Check if the player can aim (if the object is active)

            if (!isActive) { return; }

            // Gets the raw position of the mouse on the screen

            var rawMousePosition = value.Get<Vector2>();

            // Transforms the raw mouse position into world coordinates

            _mousePosition = (Vector2)_camera.ScreenToWorldPoint((Vector3)rawMousePosition);

            // Gets the value of the front of the player (positive if facing right, negative if facing left)

            Vector2 front;

            if (_movement.IsFlipped)
                front = Vector2.left;
            else
                front = Vector2.right;

            // Gets the player position

            Vector2 playerPos = transform.position;

            // Calcualtes the angle between the front of the player and the position of the mouse

            _angle = Vector2.SignedAngle(front, _mousePosition - playerPos);

        }

        // Used by the input system
        private void OnFire ()
        {
            // Fires the weapon assuming the player can fire

            if (_canFire)
                _wpnController.Fire();
        }

        // Used by the input system
        private void OnInteract ()
        {
            // Interacts with map object

            GameManager.instance.InteractionPressed();
        }

        private void UpdateDash ()
        {
            // Makes the player dash assuming it's not dashing already

            if (_isDashing)
            {
                // Disable firing during dash
                _canFire = false;

                // Computes whether the player is flipped (facing left) or not
                if (_movement.IsFlipped)
                {
                    _rb.velocity += new Vector2(_dashForce * -1f, 0f) * Time.fixedDeltaTime;
                }
                else
                {
                    _rb.velocity += new Vector2(_dashForce, 0f) * Time.fixedDeltaTime;
                }

                // Performs dash gradually (tweening)
                if (_dashTimeElapsed <= 0f)
                {
                    _dashTimeElapsed = _dashTimeTotal;
                    _isDashing = false;
                }
                else
                {
                    _dashTimeElapsed -= Time.fixedDeltaTime;
                }
            }
            else
            {
                // Enables firing after finishing dash
                _canFire = true;
            }
        }

        /*
        private void OnDrawGizmos ()
        {
            Vector2 front;

            if (_movement.IsFlipped)
                front = Vector2.left;
            else
                front = Vector2.right;

            // Vector2(x, y) = Vector2(x, y - x)

            Debug.DrawLine(transform.position, transform.position + (Vector3)front, Color.white);
            Debug.DrawLine(transform.position, _mousePosition, Color.magenta);
        } */
    }
}
