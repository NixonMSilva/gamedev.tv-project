using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Character
{
    public class WeaponController : MonoBehaviour
    {
        [SerializeField] private GameObject _firePoint;

        [SerializeField] private SpriteRenderer _spriteRenderer;

        [SerializeField] private Weapon _equippedWeapon;

        [SerializeField] private Animator _animator;

        [SerializeField] private bool _canFire = true;

        private Vector2 _front = Vector2.right;
        private Vector2 _angle = Vector2.right;

        public Vector2 Front { get; set; }

        public Vector2 Angle { get; set; }

        public bool CanFire
        {
            get => _canFire;
            set => _canFire = value;
        }

        public float WeaponCooldown
        {
            get => _equippedWeapon.wpnCooldown;
        }

        private void Start ()
        {
            _spriteRenderer.sprite = _equippedWeapon.wpnSprite;
        }

        public void Fire ()
        {
            // Doesn't fire if the flag is turned off

            if (!_canFire) { return; }

            /*
            Debug.Log("Firing " + _equippedWeapon.wpnName + " with projectile "
                + _equippedWeapon.wpnProjectile.projName + " at mode: "
                + _equippedWeapon.wpnFireMode.ToString()); */

            // Instantiates a new projectile
            var newProjectile = Instantiate(_equippedWeapon.wpnProjectile.projTemplate, _firePoint.transform.position, Quaternion.identity);
            var newProjController = newProjectile.GetComponent<ProjectileController>();

            // Sets the projectile angle based on the weapon's angle
            newProjController.Angle = Angle;

            // Sets the projectile source to the correct emitter
            newProjController.Source = gameObject;

            // Sets the animator parameter "isShooting"
            if (_animator != null)
                _animator.SetTrigger("isShooting");

            // Plays weapon sound

            AudioManager.instance.PlaySound(_equippedWeapon.wpnFireSoundName);
        }
    }
}