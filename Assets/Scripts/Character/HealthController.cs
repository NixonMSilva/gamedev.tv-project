using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;

namespace Character
{
    public class HealthController : MonoBehaviour, IDamageable
    {
        [SerializeField] private float _maxHealth;
        [SerializeField] private float _currentHealth;

        private DeathController _deathController;

        [SerializeField] private Animator _animator;

        public event Action OnDamage;
        public event Action OnDeath;

        public float Health
        {
            get { return _currentHealth; }
            set { _currentHealth = value; }
        }

        private void Start ()
        {
            Health = _maxHealth;
        }

        public void ChangeHealth (float delta)
        {
            // Change the health based on the delta received
            Health += delta;

            // Disallows health to overflow the max value
            if (Health >= _maxHealth)
                Health = _maxHealth;

            // Disallows health to underflow the min value (0)
            if (Health < 0f)
                Health = 0f;

            // In case of health decrement
            if (delta < 0f)
            {
                OnDamage?.Invoke();
            }

            // In case of death
            if (Health <= 0f)
            {
                if (_animator != null)
                    _animator.SetTrigger("isDying");

                OnDeath?.Invoke();
            }
        }

        public void TakeDamage (float damageValue)
        {
            // Decrements the health based on a damage value

            if (Health <= 0f) { return; }

            ChangeHealth(-damageValue);
        }

        [ContextMenu("Kill Character")]
        void KillCharacter ()
        {
            TakeDamage(_maxHealth);
        }
    }
}