using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Character
{
    public class StopJump : MonoBehaviour
    {
        private Animator _animator;

        private void Awake()
        {
            _animator = GetComponent<Animator>();    
        }

        public void StopJumpEvent()
        {
            if (_animator != null)
                _animator.SetBool("isJumping", false);
        }
    }
}
