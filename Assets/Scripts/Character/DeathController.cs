using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;
using Enemy;

namespace Character
{
    public class DeathController : MonoBehaviour
    {
        private HealthController _healthController;

        [SerializeField] private GameObject _ghost;
        [SerializeField] private CinemachineVirtualCamera _camera;

        [SerializeField] private PlayerController _playerController;
        [SerializeField] private PlayerInput _playerInput;

        [SerializeField] private PossessableData _possessableData;

        [SerializeField] private bool _isPlayer = false;
        [SerializeField] private bool _isPossessable = true;
        [SerializeField] private bool _isDead = false;

        public bool IsPlayer
        {
            get { return _isPlayer; }
            set { _isPlayer = value; }
        }

        public bool IsPossessable
        {
            get { return _isPossessable; }
        }

        public bool IsDead
        {
            get { return _isDead; }
        }

        private void Awake ()
        {
            // Component caching

            _healthController = GetComponent<HealthController>();

            _possessableData = GameObject.Find("PossessableData").GetComponent<PossessableData>();

            _camera = GameObject.Find("Virtual Camera").GetComponent<CinemachineVirtualCamera>();

        }

        private void OnEnable ()
        {
            _healthController.OnDeath += ProcessDeath;
        }

        private void OnDisable ()
        {
            _healthController.OnDeath -= ProcessDeath;
        }

        private void ProcessDeath ()
        {
            // Subtracts the number of possessable characters remaining in the scene

            if (_isPossessable)
                _possessableData.AddValue(-1);

            _isDead = true;
            _isPossessable = false;

            if (_isPlayer)
            {
                // Creates ghost upon death

                GameObject ghost = Instantiate(_ghost, transform.position, Quaternion.identity);

                // Makes the camera follows the ghost

                _camera.Follow = ghost.transform;

                // Unsets the player

                UnsetPlayer();
                GameManager.instance.IsPlayerAlive = false;
            }

            // Plays death sound

            AudioManager.instance.PlaySound("Machine Death");

            // GetComponentInChildren<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f);
        }

        // Process possession (when the ghost touches a possessable character)
        public void Possess ()
        {
            // Disallows possession for non-tagged characters or dead characters

            if (!_isPossessable) { return; }
            if (_isDead) { return; }

            // Debug.Log("Possession en course");

            SetPlayer();

            GameManager.instance.IsPlayerAlive = true;
        }

        public void SetPlayer ()
        {
            _isPlayer = true;

            // Attributes player characteristics to the character after a short delay

            Invoke("ActivateControls", 0.25f);

            // Makes the camera follows the new player

            _camera.Follow = gameObject.transform;

            // Destroy the AI component for the enemy

            EnemyAI _ai;
            if (TryGetComponent(out _ai))
            {
                Destroy(_ai);
            }
        }

        public void UnsetPlayer ()
        {
            _isPlayer = false;

            // Removes player characteristics from old character

            PlayerController oldPlayerController;
            PlayerInput oldPlayerInput;

            if (TryGetComponent(out oldPlayerController))
            {
                Destroy(oldPlayerController);
            }
            else
            {
                Debug.LogWarning("Couldn't delete old PlayerController");
            }

            if (TryGetComponent<PlayerInput>(out oldPlayerInput))
            {
                oldPlayerInput.enabled = false;
            }
            else
            {
                Debug.LogWarning("Couldn't delete old PlayerInput");
            }

            // Unsets the player tag
            gameObject.tag = "DeadPlayer";
        }

        // Enables the control components on the object
        private void ActivateControls ()
        {
            _playerController.enabled = true;
            _playerInput.enabled = true;
            gameObject.tag = "Player";
        }
    }
}