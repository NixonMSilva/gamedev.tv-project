using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Character
{
    public class ObjectDestroyer : MonoBehaviour
    {
        private HealthController _health;

        private void Awake()
        {
            _health = GetComponentInParent<HealthController>();
        }

        public void ObjectDestructionEvent()
        {
            // Destroy parent object upon death
            Destroy(_health.gameObject, 0.2f);
        }
    }
}
