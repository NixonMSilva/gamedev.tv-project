using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Character
{
    public class MovementController : MonoBehaviour
    {
        [SerializeField] private float _moveSpeed = 50f;
        [SerializeField] private float _jumpForce = 15f;

        [SerializeField] private bool _isGhost = false;
        [SerializeField] private bool _isFlipped = false;
        [SerializeField] private bool _isJumping = false;

        [SerializeField] private Collider2D _feet;

        [SerializeField] private Animator _animator;

        private Rigidbody2D _rb;

        private float _localScaleSize;

        private const string _walkableLayer = "Walkable";

        public bool IsFlipped { get => _isFlipped; }

        private void Awake ()
        {
            _rb = GetComponent<Rigidbody2D>();

            _localScaleSize = transform.localScale.x;
        }

        public void PerformMove (Vector2 movement)
        {
            
            // Moves the character on the X Axis if not a ghost, or XY axis if a ghost

            if (!_isGhost)
                _rb.velocity = new Vector2(movement.x * _moveSpeed, _rb.velocity.y);
            else
                _rb.velocity = new Vector2(movement.x, movement.y).normalized * _moveSpeed;

            // Updates the sprite flipping

            UpdateFlip(movement);

            // Updates animator parameter "speed"

            if (_animator != null)
                _animator.SetFloat("speed", _rb.velocity.x);
        }

        public void PerformJump ()
        {
            // Checks if the character is touching the ground
            // otherwise do not perform the jump

            if (!_feet.IsTouchingLayers(LayerMask.GetMask(_walkableLayer))) 
                return; 

            _isJumping = true;

            // Checks if the character isn't a ghost trying to jump
            // or if it isn't jumping already

            if (_isJumping && !_isGhost)
            {
                _rb.velocity += new Vector2(0f, _jumpForce);

                // Play jumping sound

                AudioManager.instance.PlaySound("Jump");

                // Updates animator parameter "isJumping"

                if (_animator != null)
                    _animator.SetBool("isJumping", true);

                _isJumping = false;
            }
            
        }

        private void UpdateFlip (Vector2 movement)
        {
            // Flip the sprite according to the movement

            if (movement.x > 0)
            {
                Flip(false);
            }
            else if (movement.x < 0)
            {
                Flip(true);
            }
        }

        public void Flip (bool flipStatus)
        {
            if (!flipStatus)
            {
                _isFlipped = false;
                transform.localScale = new Vector3(_localScaleSize, transform.localScale.y, transform.localScale.z);
            }
            else
            {
                _isFlipped = true;
                transform.localScale = new Vector3(-_localScaleSize, transform.localScale.y, transform.localScale.z);
            }
        }

        public void Stop ()
        {
            _rb.velocity = new Vector2(0f, _rb.velocity.y);
        }
    }
}