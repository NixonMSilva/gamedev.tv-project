﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MapObjects
{
    public class MapDoor : MonoBehaviour
    {
        [SerializeField] private MapButton _buttonReference;

        private Animator _animator;

        [SerializeField] protected bool _isOpen = false;

        [SerializeField] protected Collider2D _doorCollider;

        private void Awake ()
        {
            _animator = GetComponent<Animator>();            
        }

        private void OnEnable()
        {
            _buttonReference.OnPress += OpenDoor;
        }

        private void OnDisable()
        {
            _buttonReference.OnPress -= OpenDoor;
        }

        private void OpenDoor()
        {
            if (!_isOpen)
            {
                AudioManager.instance.PlaySound("Door");
                _animator.SetTrigger("isOpening");
                _isOpen = true;
            }
        }
    }
}