﻿using UnityEditor;
using UnityEngine;

namespace MapObjects
{
    [RequireComponent(typeof(Collider2D))]
    public abstract class MapTooltip : MonoBehaviour
    {
        [SerializeField] protected string _tooltipText = "Tooltip Text";

        protected abstract void OnTriggerEnter2D (Collider2D other);

        protected abstract void OnTriggerExit2D (Collider2D other);
    }
}