﻿using UnityEditor;
using UnityEngine;

namespace MapObjects
{
    public class MapMessageTooltip : MapTooltip
    {
        protected override void OnTriggerEnter2D (Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
                GameManager.instance.UI.ShowTooltip(_tooltipText);
        }

        protected override void OnTriggerExit2D (Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                GameManager.instance.UI.HideTooltip();
                Destroy(gameObject, 0.1f);
            }
        }
    }
}