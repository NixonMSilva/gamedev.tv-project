using System;
using UnityEngine;


namespace MapObjects
{
    public class MapButton : MapTooltip, IInteractable
    {
        private Animator _animator;

        private GameObject _playerReference;

        // [SerializeField] private string _tooltipText = "Press Button";

        [SerializeField] private float _interactionDistance = 2f;
        [SerializeField] private bool _isPressed = false;

        public event Action OnPress;

        private void Awake ()
        {
            _animator = GetComponent<Animator>();
        }

        private void OnEnable()
        {
            GameManager.instance.onInteraction += Interact;
        }

        private void OnDisable()
        {
            GameManager.instance.onInteraction -= Interact;
        }

        public void Interact ()
        {
            // Checks if the button isn't pressed and if the player
            // is in range for interaction

            if (!_isPressed && IsPlayerInRange())
                PressButton();
        }

        private bool IsPlayerInRange ()
        {
            // Updates player reference from gamemanager and check
            // if it's in range for interaction

            _playerReference = GameManager.instance.PlayerObject;
            if (Vector2.Distance(_playerReference.transform.position, 
                gameObject.transform.position) 
                <= _interactionDistance)
                return true;
            return false;
        }

        private void PressButton ()
        {
            OnPress.Invoke();
            _animator.SetTrigger("isPressed");
            _isPressed = true;
            GameManager.instance.UI.HideTooltip();
            AudioManager.instance.PlaySound("Button Press");
        }

        protected override void OnTriggerEnter2D (Collider2D other)
        {
            if (other.gameObject.CompareTag("Player") && !_isPressed)
                GameManager.instance.UI.ShowTooltip(_tooltipText);
        }

        protected override void OnTriggerExit2D (Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
                GameManager.instance.UI.HideTooltip();
        }
    }
}
