using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Character;

namespace MapObjects
{
    public class MapAcid : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Enemy"))
            {
                collision.gameObject.GetComponent<HealthController>().TakeDamage(999f);
            }
        }
    }
}
