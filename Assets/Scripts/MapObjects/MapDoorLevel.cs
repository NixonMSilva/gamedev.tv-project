using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MapObjects
{
    public class MapDoorLevel : MapDoor
    {
        [SerializeField] private string _nextLevelName = "";

        private void OnTriggerEnter2D (Collider2D collision)
        {
            // If the object colliding is the player and the door leads to another level
            // then call scene manager and load the new scene (level)

            if (collision.gameObject.CompareTag("Player") && _isOpen)
            {
                // Loads a new scene after a short delay

                Invoke("LoadNewScene", 0.25f);
            }
        }

        private void LoadNewScene ()
        {
            SceneManager.LoadScene(_nextLevelName);
        }
    }
}
