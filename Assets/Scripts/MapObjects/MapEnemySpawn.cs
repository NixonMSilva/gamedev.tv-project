using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Character;

namespace MapObjects
{
    public class MapEnemySpawn : MonoBehaviour
    {
        [SerializeField] private GameObject _enemyTemplate;

        [SerializeField] private PossessableData _possessableData;

        [SerializeField] private float _spawnCooldown = 10f;
        private float _spawnCooldownElapsed = 0f;

        private void Update ()
        {
            if (_spawnCooldownElapsed >= _spawnCooldown)
            {
                _spawnCooldownElapsed = 0f;
                SpawnEnemy();
            }
            _spawnCooldownElapsed += Time.deltaTime;
        }

        private void SpawnEnemy ()
        {
            // Only spawns if there are two enemies or less in the screen

            if (_possessableData.PossessableCount <= 3)
            {
                Instantiate(_enemyTemplate, transform.position, Quaternion.identity);
                _possessableData.AddValue(1);
            }
        }
    }
}
