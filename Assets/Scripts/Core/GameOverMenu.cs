using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    public void Continue ()
    {
        int level = GameManager.instance.CurrentLevel;
        GameManager.instance.IsPlayerAlive = true;
        if (level > 1)
            SceneManager.LoadScene("Level " + GameManager.instance.CurrentLevel);
        else
            SceneManager.LoadScene("Tutorial");
    }

    public void Exit ()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
