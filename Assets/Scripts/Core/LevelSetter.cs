using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSetter : MonoBehaviour
{
    [SerializeField] private int _levelIndex;

    // Start is called before the first frame update
    private void Start ()
    {
        Debug.Log("Level setter called!");

        // Sets the level on the GameManager and destroys this object
        GameManager.instance.PlayerObject = GameObject.Find("Player");
        GameManager.instance.UI = GameObject.Find("UI").GetComponent<UIController>();

        GameManager.instance.CurrentLevel = _levelIndex;
        Destroy(gameObject, 0.1f);
    }

}
