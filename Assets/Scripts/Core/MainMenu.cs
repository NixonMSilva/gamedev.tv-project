using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private Animator _animator;

    [SerializeField] private GameObject _optionsMenu;

    private void Awake()
    {
        // Caching components

        _animator = GetComponent<Animator>();
    }

    public void StartAnimation ()
    {
        _animator.SetTrigger("start");
    }

    public void QuitAnimation ()
    {
        _animator.SetTrigger("quit");
    }

    public void OptionsAnimation ()
    {
        _animator.SetTrigger("options");
    }

    public void CreditsAnimation()
    {
        _animator.SetBool("credits", true);
    }

    public void StartEvent ()
    {
        SceneManager.LoadScene("IntroScreen");
    }

    public void QuitEvent ()
    {
        Application.Quit();
    }

    public void OptionsEvent ()
    {
        _optionsMenu.SetActive(true);
        this.gameObject.SetActive(false);
    }

    public void CreditsEvent ()
    {
        _animator.SetBool("credits", false);
    }
}
