using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    [SerializeField] AudioSource _music;

    private void Awake()
    {
        _music = GetComponent<AudioSource>();    
    }

    private void Start()
    {
        _music.volume = 0.5f * AudioManager.instance.GlobalVolume;
        _music.Play();
    }
}
