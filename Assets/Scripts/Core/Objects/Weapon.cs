using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Datasets/Weapon")]
public class Weapon : ScriptableObject
{
    public int id;
    public string wpnName;
    public string wpnFireSoundName;
    public Sprite wpnSprite;
    public Projectile wpnProjectile;
    public FireMode wpnFireMode;
    public float wpnCooldown;
}
