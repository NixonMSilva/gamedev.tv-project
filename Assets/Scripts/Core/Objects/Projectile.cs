using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Projectile", menuName = "Datasets/Projectile")]
public class Projectile : ScriptableObject
{
    public int id;
    public string projName;
    public GameObject projTemplate;
    public float damage;
    public float speed;
}
