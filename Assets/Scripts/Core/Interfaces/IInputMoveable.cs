using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;

public interface IInputMoveable
{
    void OnMove (InputValue value);
}
