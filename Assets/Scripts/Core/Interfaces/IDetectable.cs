﻿using UnityEditor;
using UnityEngine;

public interface IDetectable
{
    void DetectDamage(GameObject source);
}