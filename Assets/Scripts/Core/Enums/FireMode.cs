using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FireMode
{
    singleShot, burstShot, arcedShot, none
}