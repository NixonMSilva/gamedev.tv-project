using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField] private GameObject _playerObject;

    [SerializeField] private UIController _uiController;

    private bool _playerAliveStatus;

    private bool _hasDiedOnce = false;
    private bool _hasPossessedOnce = false;

    private int _currentLevel = 0;

    public event Action<GameObject> onPlayerChange;
    public event Action onInteraction;

    public GameObject PlayerObject
    {
        get => _playerObject;
        set
        {
            _playerObject = value;
            onPlayerChange?.Invoke(_playerObject);
        }
    }

    public bool IsPlayerAlive
    {
        get => _playerAliveStatus;
        set
        {
            _playerAliveStatus = value;

            if (_playerAliveStatus == false && _hasDiedOnce == false)
            {
                _hasDiedOnce = true;
                GameManager.instance.UI.ShowTooltip("Possess another body to keep going! Try walking into them!");
            }

            if (_playerAliveStatus == true && _hasPossessedOnce == false)
            {
                _hasPossessedOnce = true;
                GameManager.instance.UI.HideTooltip();
            }
        }
    }

    public int CurrentLevel
    {
        get => _currentLevel;
        set => _currentLevel = value;
    }

    public UIController UI
    {
        get => _uiController;
        set => _uiController = value;
    }

    private void Awake ()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        // Debug.Log("Instance status on GameManager.cs: " + (instance != null));

        DontDestroyOnLoad(gameObject);
    }

    private void Start ()
    {
        // Might need to remove later

        _playerObject = GameObject.FindGameObjectWithTag("Player");

        _playerAliveStatus = true;

        // Initial player reference

        instance.onPlayerChange?.Invoke(_playerObject);

    }

    public void Defeat ()
    {
        // Debug.Log("Defeat condition called!");
        SceneManager.LoadScene("GameOver");
    }

    public void InteractionPressed()
    {
        onInteraction.Invoke();
    }
}
