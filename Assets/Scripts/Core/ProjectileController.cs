using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidBody;
    [SerializeField] private Collider2D _collider;

    [SerializeField] private Projectile _projectileData;

    // Projectile caster
    private GameObject _source = null;

    // Projectile angle of translation
    private Vector2 _angle = Vector2.right;

    public GameObject Source
    {
        get { return _source; }
        set { _source = value;}
    }

    public Vector2 Angle 
    { 
        get { return _angle; }
        set { _angle = value; }
    }

    private void Awake ()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _collider = GetComponent<CircleCollider2D>();
    }

    private void FixedUpdate ()
    {
        Translate();
    }

    // Translates the projectile onto the screen based on its data
    private void Translate ()
    {
        _rigidBody.MovePosition(_rigidBody.position + (Angle.normalized * _projectileData.speed * Time.fixedDeltaTime));
    }

    private void OnTriggerEnter2D (Collider2D collision)
    {
        // Collision with solid objects

        if (collision.gameObject.CompareTag("Solid"))
        {
            Destroy(this.gameObject);
        }

        // Collision with damageable objects that aren't the source or doesn't
        // have the same tag as the source (e.g. to avoid enemies killing their allies)

        if (this.gameObject != null && collision != null
            && collision.gameObject.layer == LayerMask.NameToLayer("Damageable")
            && !collision.gameObject.CompareTag(_source.gameObject.tag))
        {
            // Deals damage to any target with the interface IDamageable
            var damageable = collision.gameObject.GetComponent<IDamageable>();

            if (damageable != null)
            {
                damageable.TakeDamage(_projectileData.damage);
            }

            var detectable = collision.gameObject.GetComponent<IDetectable>();

            if (detectable != null)
            {
                detectable.DetectDamage(_source);
            }

            Destroy(this.gameObject);
        }
    }
}
