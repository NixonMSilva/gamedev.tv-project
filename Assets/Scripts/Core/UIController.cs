using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _tooltip;

    public void ShowTooltip (string tooltipText)
    {
        _tooltip.gameObject.SetActive(true);
        _tooltip.text = tooltipText;
    }

    public void HideTooltip ()
    {
        _tooltip.text = "";
        _tooltip.gameObject.SetActive(false);
    }
    
}
