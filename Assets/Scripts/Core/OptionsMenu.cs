using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField] private GameObject _mainMenu;

    [SerializeField] private Slider _volumeSlider;

    private void Start ()
    {
        _volumeSlider.value = AudioManager.instance.GlobalVolume * 100f;
    }

    public void ChangeMasterVolume ()
    {
        AudioManager.instance.GlobalVolume = (_volumeSlider.value / 100f);
    }

    public void ReturnToMainMenu ()
    {
        _mainMenu.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
